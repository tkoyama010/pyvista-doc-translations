.. only:: html

    .. note::
        :class: sphx-glr-download-link-note

        Click :ref:`here <sphx_glr_download_examples_00-load_read-dolfin.py>`     to download the full example code
    .. rst-class:: sphx-glr-example-title

    .. _sphx_glr_examples_00-load_read-dolfin.py:


Read FEniCS/Dolfin Meshes
~~~~~~~~~~~~~~~~~~~~~~~~~

PyVista leverages `meshio`_ to read many mesh formats not natively supported
by VTK including the `FEniCS/Dolfin`_ XML format.

.. _meshio: https://github.com/nschloe/meshio
.. _FEniCS/Dolfin: https://fenicsproject.org


.. code-block:: default

    import pyvista as pv
    from pyvista import examples








Let's download an example FEniCS/Dolfin mesh from our example data
repository. This will download an XML Dolfin mesh and save it to PyVista's
data directory.


.. code-block:: default

    saved_file, _ = examples.downloads._download_file("dolfin_fine.xml")
    print(saved_file)





.. rst-class:: sphx-glr-script-out

 Out:

 .. code-block:: none

    /home/runner/.local/share/pyvista/examples/dolfin_fine.xml




As shown, we now have an XML Dolfin mesh save locally. This filename can be
passed directly to PyVista's :func:`pyvista.read` method to be read into
a PyVista mesh.


.. code-block:: default

    dolfin = pv.read(saved_file)
    dolfin







.. raw:: html


    <table>
    <tr><th>UnstructuredGrid</th><th>Information</th></tr>
    <tr><td>N Cells</td><td>5400</td></tr>
    <tr><td>N Points</td><td>2868</td></tr>
    <tr><td>X Bounds</td><td>0.000e+00, 1.000e+00</td></tr>
    <tr><td>Y Bounds</td><td>0.000e+00, 1.000e+00</td></tr>
    <tr><td>Z Bounds</td><td>0.000e+00, 0.000e+00</td></tr>
    <tr><td>N Arrays</td><td>0</td></tr>
    </table>


    <br />
    <br />

Now we can do stuff with that Dolfin mesh!


.. code-block:: default

    qual = dolfin.compute_cell_quality()
    qual.plot(show_edges=True, cpos="xy")



.. image:: /examples/00-load/images/sphx_glr_read-dolfin_001.png
    :alt: read dolfin
    :class: sphx-glr-single-img


.. rst-class:: sphx-glr-script-out

 Out:

 .. code-block:: none


    [(0.5, 0.5, 2.7320508075688776),
     (0.5, 0.5, 0.0),
     (0.0, 1.0, 0.0)]




.. rst-class:: sphx-glr-timing

   **Total running time of the script:** ( 0 minutes  1.970 seconds)


.. _sphx_glr_download_examples_00-load_read-dolfin.py:


.. only :: html

 .. container:: sphx-glr-footer
    :class: sphx-glr-footer-example



  .. container:: sphx-glr-download sphx-glr-download-python

     :download:`Download Python source code: read-dolfin.py <read-dolfin.py>`



  .. container:: sphx-glr-download sphx-glr-download-jupyter

     :download:`Download Jupyter notebook: read-dolfin.ipynb <read-dolfin.ipynb>`


.. only:: html

 .. rst-class:: sphx-glr-signature

    `Gallery generated by Sphinx-Gallery <https://sphinx-gallery.github.io>`_
