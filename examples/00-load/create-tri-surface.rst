.. only:: html

    .. note::
        :class: sphx-glr-download-link-note

        Click :ref:`here <sphx_glr_download_examples_00-load_create-tri-surface.py>`     to download the full example code
    .. rst-class:: sphx-glr-example-title

    .. _sphx_glr_examples_00-load_create-tri-surface.py:


Create Triangulated Surface
~~~~~~~~~~~~~~~~~~~~~~~~~~~

Create a surface from a set of points through a Delaunay triangulation.


.. code-block:: default


    # sphinx_gallery_thumbnail_number = 2
    import pyvista as pv
    import numpy as np








Simple Traingulations
+++++++++++++++++++++

First, create some points for the surface.


.. code-block:: default


    # Define a simple Gaussian surface
    n = 20
    x = np.linspace(-200, 200, num=n) + np.random.uniform(-5, 5, size=n)
    y = np.linspace(-200, 200, num=n) + np.random.uniform(-5, 5, size=n)
    xx, yy = np.meshgrid(x, y)
    A, b = 100, 100
    zz = A * np.exp(-0.5 * ((xx / b) ** 2.0 + (yy / b) ** 2.0))

    # Get the points as a 2D NumPy array (N by 3)
    points = np.c_[xx.reshape(-1), yy.reshape(-1), zz.reshape(-1)]
    points[0:5, :]





.. rst-class:: sphx-glr-script-out

 Out:

 .. code-block:: none


    array([[-203.54089304, -197.98916396,    1.77489271],
           [-183.57915428, -197.98916396,    2.61200843],
           [-155.11541148, -197.98916396,    4.22975973],
           [-132.45566691, -197.98916396,    5.85888638],
           [-119.4607915 , -197.98916396,    6.90082313]])



Now use those points to create a point cloud PyVista data object. This will
be encompassed in a :class:`pyvista.PolyData` object.


.. code-block:: default


    # simply pass the numpy points to the PolyData constructor
    cloud = pv.PolyData(points)
    cloud.plot(point_size=15)




.. image:: /examples/00-load/images/sphx_glr_create-tri-surface_001.png
    :alt: create tri surface
    :class: sphx-glr-single-img


.. rst-class:: sphx-glr-script-out

 Out:

 .. code-block:: none


    [(642.0231405746704, 646.3957049129399, 694.3725690882031),
     (-1.8171255465450002, 2.5554387917246117, 50.53230296698786),
     (0.0, 0.0, 1.0)]



Now that we have a PyVista data structure of the points, we can perform a
triangulation to turn those boring discrete points into a connected surface.


.. code-block:: default


    surf = cloud.delaunay_2d()
    surf.plot(show_edges=True)





.. image:: /examples/00-load/images/sphx_glr_create-tri-surface_002.png
    :alt: create tri surface
    :class: sphx-glr-single-img


.. rst-class:: sphx-glr-script-out

 Out:

 .. code-block:: none


    [(642.0231405746704, 646.3957049129399, 694.3725690882031),
     (-1.8171255465450002, 2.5554387917246117, 50.53230296698786),
     (0.0, 0.0, 1.0)]



Masked Triangulations
+++++++++++++++++++++



.. code-block:: default


    x = np.arange(10, dtype=float)
    xx, yy, zz = np.meshgrid(x, x, [0])
    points = np.column_stack((xx.ravel(order="F"),
                              yy.ravel(order="F"),
                              zz.ravel(order="F")))
    # Perturb the points
    points[:, 0] += np.random.rand(len(points)) * 0.3
    points[:, 1] += np.random.rand(len(points)) * 0.3
    # Create the point cloud mesh to triangulate from the coordinates
    cloud = pv.PolyData(points)
    cloud






.. raw:: html


    <table>
    <tr><th>PolyData</th><th>Information</th></tr>
    <tr><td>N Cells</td><td>100</td></tr>
    <tr><td>N Points</td><td>100</td></tr>
    <tr><td>X Bounds</td><td>5.664e-03, 9.260e+00</td></tr>
    <tr><td>Y Bounds</td><td>3.322e-02, 9.299e+00</td></tr>
    <tr><td>Z Bounds</td><td>0.000e+00, 0.000e+00</td></tr>
    <tr><td>N Arrays</td><td>0</td></tr>
    </table>


    <br />
    <br />

Run the triangulation on these points


.. code-block:: default

    surf = cloud.delaunay_2d()
    surf.plot(cpos="xy", show_edges=True)





.. image:: /examples/00-load/images/sphx_glr_create-tri-surface_003.png
    :alt: create tri surface
    :class: sphx-glr-single-img


.. rst-class:: sphx-glr-script-out

 Out:

 .. code-block:: none


    [(4.632680506639189, 4.665915385758183, 25.298016034135227),
     (4.632680506639189, 4.665915385758183, 0.0),
     (0.0, 1.0, 0.0)]



Note that some of the outer edges are unconstrained and the triangulation
added unwanted triangles. We cn mitigate that with the ``alpha`` parameter.


.. code-block:: default

    surf = cloud.delaunay_2d(alpha=1.0)
    surf.plot(cpos="xy", show_edges=True)





.. image:: /examples/00-load/images/sphx_glr_create-tri-surface_004.png
    :alt: create tri surface
    :class: sphx-glr-single-img


.. rst-class:: sphx-glr-script-out

 Out:

 .. code-block:: none


    [(4.632680506639189, 4.665915385758183, 25.298016034135227),
     (4.632680506639189, 4.665915385758183, 0.0),
     (0.0, 1.0, 0.0)]



We could also add a polygon to ignore during the triangulation via the
``edge_source`` parameter.


.. code-block:: default


    # Define a polygonal hole with a clockwise polygon
    ids = [22, 23, 24, 25, 35, 45, 44, 43, 42, 32]

    # Create a polydata to store the boundary
    polygon = pv.PolyData()
    # Make sure it has the same points as the mesh being triangulated
    polygon.points = points
    # But only has faces in regions to ignore
    polygon.faces = np.array([len(ids),] + ids)

    surf = cloud.delaunay_2d(alpha=1.0, edge_source=polygon)

    p = pv.Plotter()
    p.add_mesh(surf, show_edges=True)
    p.add_mesh(polygon, color="red", opacity=0.5)
    p.show(cpos="xy")



.. image:: /examples/00-load/images/sphx_glr_create-tri-surface_005.png
    :alt: create tri surface
    :class: sphx-glr-single-img


.. rst-class:: sphx-glr-script-out

 Out:

 .. code-block:: none


    [(4.632680506639189, 4.665915385758183, 25.298016034135227),
     (4.632680506639189, 4.665915385758183, 0.0),
     (0.0, 1.0, 0.0)]




.. rst-class:: sphx-glr-timing

   **Total running time of the script:** ( 0 minutes  3.570 seconds)


.. _sphx_glr_download_examples_00-load_create-tri-surface.py:


.. only :: html

 .. container:: sphx-glr-footer
    :class: sphx-glr-footer-example



  .. container:: sphx-glr-download sphx-glr-download-python

     :download:`Download Python source code: create-tri-surface.py <create-tri-surface.py>`



  .. container:: sphx-glr-download sphx-glr-download-jupyter

     :download:`Download Jupyter notebook: create-tri-surface.ipynb <create-tri-surface.ipynb>`


.. only:: html

 .. rst-class:: sphx-glr-signature

    `Gallery generated by Sphinx-Gallery <https://sphinx-gallery.github.io>`_
