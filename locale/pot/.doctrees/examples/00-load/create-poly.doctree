��++      �sphinx.addnodes��document���)��}�(�	rawsource�� ��children�]�(h �only���)��}�(hhh]�(�docutils.nodes��note���)��}�(h�kClick :ref:`here <sphx_glr_download_examples_00-load_create-poly.py>`     to download the full example code�h]�h�	paragraph���)��}�(hhh]�(h�Text����Click �����}�(h�Click ��parent�hubh �pending_xref���)��}�(h�?:ref:`here <sphx_glr_download_examples_00-load_create-poly.py>`�h]�h�inline���)��}�(hh&h]�h�here�����}�(hhh!h*uba�
attributes�}�(�ids�]��classes�]�(�xref��std��std-ref�e�names�]��dupnames�]��backrefs�]�u�tagname�h(h!h$ubah1}�(h3]�h5]�h:]�h<]�h>]��refdoc��examples/00-load/create-poly��	refdomain�h8�reftype��ref��refexplicit���refwarn���	reftarget��1sphx_glr_download_examples_00-load_create-poly.py�uh@h"�source��q/home/runner/work/pyvista-doc-translations/pyvista-doc-translations/pyvista/docs/examples/00-load/create-poly.rst��line�Kh!hubh�&     to download the full example code�����}�(h�&     to download the full example code�h!hubeh1}�(h3]�h5]�h:]�h<]�h>]�uh@hhPhQhRKh!h�uid�� ae86d9c8230945128d69ba43afd4b982�ubah1}�(h3]�h5]��sphx-glr-download-link-note�ah:]�h<]�h>]�uh@hh!hhhhPhQhRNubh�target���)��}�(h�-.. _sphx_glr_examples_00-load_create-poly.py:�h]�h1}�(h3]�h5]�h:]�h<]�h>]��refid��(sphx-glr-examples-00-load-create-poly-py�uh@hghRK	h!hhhhPhQubeh1}�(h3]�h5]�h:]�h<]�h>]��expr��html�uh@h	hhhPhQhRKh!hubhh)��}�(h�.. _ref_create_poly:�h]�h1}�(h3]�h5]�h:]�h<]�h>]�hs�ref-create-poly�uh@hghRKh!hhhhPhQ�expect_referenced_by_name�}��(sphx_glr_examples_00-load_create-poly.py�his�expect_referenced_by_id�}�hthisubh�section���)��}�(hhh]�(h�title���)��}�(h�Create PolyData�h]�h�Create PolyData�����}�(hh�h!h�hhhPNhRNubah1}�(h3]�h5]�h:]�h<]�h>]�uh@h�h!h�hhhPhQhRKh^� fae41be915d14d3389943cfa893e4d15�ubh)��}�(h�^Creating a PolyData (triangulated surface) object from NumPy arrays of the
vertices and faces.�h]�h�^Creating a PolyData (triangulated surface) object from NumPy arrays of the
vertices and faces.�����}�(hh�h!h�hhhPNhRNubah1}�(h3]�h5]�h:]�h<]�h>]�uh@hhPhQhRKh!h�hhh^� a0e015cefe854bd69858193d68e1869c�ubh�literal_block���)��}�(h�'import numpy as np
import pyvista as pv�h]�h�'import numpy as np
import pyvista as pv�����}�(hhh!h�ubah1}�(h3]�h5]�h:]�h<]�h>]��	xml:space��preserve��force���language��default��highlight_args�}�uh@h�hPhQhRKh!h�hhubh)��}�(h��A PolyData object can be created quickly from numpy arrays.  The vertex array
contains the locations of the points in the mesh and the face array contains
the number of points of each face and the indices of the vertices which comprise that face.�h]�h��A PolyData object can be created quickly from numpy arrays.  The vertex array
contains the locations of the points in the mesh and the face array contains
the number of points of each face and the indices of the vertices which comprise that face.�����}�(hh�h!h�hhhPNhRNubah1}�(h3]�h5]�h:]�h<]�h>]�uh@hhPhQhRK#h!h�hhh^� 2f8350b518ef4a269d0fd4d41cfd1223�ubh�)��}�(hX�  # mesh points
vertices = np.array([[0, 0, 0],
                     [1, 0, 0],
                     [1, 1, 0],
                     [0, 1, 0],
                     [0.5, 0.5, -1]])

# mesh faces
faces = np.hstack([[4, 0, 1, 2, 3],  # square
                   [3, 0, 1, 4],     # triangle
                   [3, 1, 2, 4]])    # triangle

surf = pv.PolyData(vertices, faces)

# plot each face with a different color
surf.plot(scalars=np.arange(3), cpos=[-1, 1, 0.5])�h]�hX�  # mesh points
vertices = np.array([[0, 0, 0],
                     [1, 0, 0],
                     [1, 1, 0],
                     [0, 1, 0],
                     [0.5, 0.5, -1]])

# mesh faces
faces = np.hstack([[4, 0, 1, 2, 3],  # square
                   [3, 0, 1, 4],     # triangle
                   [3, 1, 2, 4]])    # triangle

surf = pv.PolyData(vertices, faces)

# plot each face with a different color
surf.plot(scalars=np.arange(3), cpos=[-1, 1, 0.5])�����}�(hhh!h�ubah1}�(h3]�h5]�h:]�h<]�h>]�h�h�hĉhŌdefault�h�}�uh@h�hPhQhRK(h!h�hhubh�image���)��}�(h�x.. image:: /examples/00-load/images/sphx_glr_create-poly_001.png
    :alt: create poly
    :class: sphx-glr-single-img

�h]�h1}�(h3]�h5]��sphx-glr-single-img�ah:]�h<]�h>]��alt��create poly��uri��4examples/00-load/images/sphx_glr_create-poly_001.png��
candidates�}��*�h�suh@h�h!h�hhhPhQhRNubh)��}�(h�Out:�h]�h�Out:�����}�(hh�h!h�hhhPNhRNubah1}�(h3]�h5]��sphx-glr-script-out�ah:]�h<]�h>]�uh@hhPhQhRKEh!h�hhh^� 1f0acbb871314dc999a71be58a8b8b01�ubh�)��}�(h�c[(-1.7307101433008212, 2.730710143300821, 0.6153550716504106),
 (0.5, 0.5, -0.5),
 (0.0, 0.0, 1.0)]�h]�h�c[(-1.7307101433008212, 2.730710143300821, 0.6153550716504106),
 (0.5, 0.5, -0.5),
 (0.0, 0.0, 1.0)]�����}�(hhh!j  ubah1}�(h3]�h5]�j  ah:]�h<]�h>]�h�h�hĉhŌnone�h�}�uh@h�hPhQhRKGh!h�hhubh)��}�(h�A**Total running time of the script:** ( 0 minutes  0.490 seconds)�h]�(h�strong���)��}�(h�%**Total running time of the script:**�h]�h�!Total running time of the script:�����}�(hhh!j"  ubah1}�(h3]�h5]�h:]�h<]�h>]�uh@j   h!j  ubh� ( 0 minutes  0.490 seconds)�����}�(h� ( 0 minutes  0.490 seconds)�h!j  hhhPNhRNubeh1}�(h3]�h5]��sphx-glr-timing�ah:]�h<]�h>]�uh@hhPhQhRKSh!h�hhh^� 9e9070fb9aef4b4084ebfedf14c72cf0�ubhh)��}�(h�6.. _sphx_glr_download_examples_00-load_create-poly.py:�h]�h1}�(h3]�h5]�h:]�h<]�h>]�hs�1sphx-glr-download-examples-00-load-create-poly-py�uh@hghRKVh!h�hhhPhQubh
)��}�(hhh]�h�	container���)��}�(hX  .. container:: sphx-glr-download sphx-glr-download-python

   :download:`Download Python source code: create-poly.py <create-poly.py>`



.. container:: sphx-glr-download sphx-glr-download-jupyter

   :download:`Download Jupyter notebook: create-poly.ipynb <create-poly.ipynb>`�h]�(jL  )��}�(h�H:download:`Download Python source code: create-poly.py <create-poly.py>`�h]�h)��}�(hjS  h]�h �download_reference���)��}�(hjS  h]�h�literal���)��}�(hjS  h]�h�+Download Python source code: create-poly.py�����}�(hhh!j_  ubah1}�(h3]�h5]�(h7�download�eh:]�h<]�h>]�uh@j]  h!jZ  ubah1}�(h3]�h5]�h:]�h<]�h>]��refdoc�hH�	refdomain�h�reftype�ji  �refexplicit���refwarn��hN�create-poly.py��filename��/a9f467bdb77ddfaee139b379ab04234c/create-poly.py�uh@jX  hPhQhRKbh!jU  ubah1}�(h3]�h5]�h:]�h<]�h>]�uh@hhPhQhRKbh!jQ  h^� cff7bb128d4f4e2a9858bfe308b558c0�ubah1}�(h3]�h5]�(�sphx-glr-download��sphx-glr-download-python�eh:]�h<]�h>]�uh@jK  h!jM  ubjL  )��}�(h�L:download:`Download Jupyter notebook: create-poly.ipynb <create-poly.ipynb>`�h]�h)��}�(hj�  h]�jY  )��}�(hj�  h]�j^  )��}�(hj�  h]�h�,Download Jupyter notebook: create-poly.ipynb�����}�(hhh!j�  ubah1}�(h3]�h5]�(h7�download�eh:]�h<]�h>]�uh@j]  h!j�  ubah1}�(h3]�h5]�h:]�h<]�h>]��refdoc�hH�	refdomain�h�reftype�j�  �refexplicit���refwarn��hN�create-poly.ipynb�jy  �2d80e588fd734a20344d2b15ba54e0524/create-poly.ipynb�uh@jX  hPhQhRKhh!j�  ubah1}�(h3]�h5]�h:]�h<]�h>]�uh@hhPhQhRKhh!j�  h^� f9b858f3ad2f46e39a29d3f4afe0f623�ubah1}�(h3]�h5]�(�sphx-glr-download��sphx-glr-download-jupyter�eh:]�h<]�h>]�uh@jK  h!jM  ubeh1}�(h3]�h5]�(�sphx-glr-footer��class��sphx-glr-footer-example�eh:]�h<]�h>]�uh@jK  h!jH  hhhPNhRNubah1}�(h3]�jG  ah5]�h:]��1sphx_glr_download_examples_00-load_create-poly.py�ah<]�h>]�h{�html�uh@h	hhhPhQhRKYh!h�h�}�j�  j=  sh�}�jG  j=  subh
)��}�(hhh]�h)��}�(h�I`Gallery generated by Sphinx-Gallery <https://sphinx-gallery.github.io>`_�h]�(h�	reference���)��}�(hj�  h]�h�#Gallery generated by Sphinx-Gallery�����}�(h�#Gallery generated by Sphinx-Gallery�h!j�  ubah1}�(h3]�h5]�h:]�h<]�h>]��name��#Gallery generated by Sphinx-Gallery��refuri�� https://sphinx-gallery.github.io�uh@j�  h!j�  ubhh)��}�(h�# <https://sphinx-gallery.github.io>�h]�h1}�(h3]��#gallery-generated-by-sphinx-gallery�ah5]�h:]��#gallery generated by sphinx-gallery�ah<]�h>]��refuri�j�  uh@hg�
referenced�Kh!j�  ubeh1}�(h3]�h5]��sphx-glr-signature�ah:]�h<]�h>]�uh@hhPhQhRKoh!j�  hhh^� d100f899444f4e88a538270c3e7623c1�ubah1}�(h3]�h5]�h:]�h<]�h>]�h{�html�uh@h	hhhPhQhRKkh!h�ubeh1}�(h3]�(�create-polydata�h�hteh5]��sphx-glr-example-title�ah:]�(�create polydata��ref_create_poly�h�eh<]�h>]�uh@h�h!hhhhPhQhRKh�}�(j  h}h�hiuh�}�(h�h}hthiuubeh1}�(h3]�h5]�h:]�h<]�h>]��source�hQuh@h�current_source�N�current_line�N�settings��docutils.frontend��Values���)��}�(h�N�	generator�N�	datestamp�N�source_link�N�
source_url�N�toc_backlinks��entry��footnote_backlinks�K�sectnum_xform�K�strip_comments�N�strip_elements_with_classes�N�strip_classes�N�report_level�K�
halt_level�K�exit_status_level�K�debug�N�warning_stream�N�	traceback���input_encoding��	utf-8-sig��input_encoding_error_handler��strict��output_encoding��utf-8��output_encoding_error_handler�j8  �error_encoding��UTF-8��error_encoding_error_handler��backslashreplace��language_code��en��record_dependencies�N�config�N�	id_prefix�h�auto_id_prefix��id��dump_settings�N�dump_internals�N�dump_transforms�N�dump_pseudo_xml�N�expose_internals�N�strict_visitor�N�_disable_config�N�_source�hQ�_destination�N�_config_files�]��pep_references�N�pep_base_url�� https://www.python.org/dev/peps/��pep_file_url_template��pep-%04d��rfc_references�N�rfc_base_url��https://tools.ietf.org/html/��	tab_width�K�trim_footnote_reference_space���file_insertion_enabled���raw_enabled�K�syntax_highlight��long��smart_quotes���smartquotes_locales�]��character_level_inline_markup���doctitle_xform���docinfo_xform�K�sectsubtitle_xform���embed_stylesheet���cloak_email_addresses���env�Nub�reporter�N�indirect_targets�]��substitution_defs�}��substitution_names�}��refnames�}��refids�}�(ht]�hiah�]�h}ajG  ]�j=  au�nameids�}�(h�htj  h�j  j  j�  jG  j�  j�  u�	nametypes�}�(h��j  �j  Nj�  �j�  �uh3}�(hth�h�h�j  h�jG  jH  j�  j�  u�footnote_refs�}��citation_refs�}��autofootnotes�]��autofootnote_refs�]��symbol_footnotes�]��symbol_footnote_refs�]��	footnotes�]��	citations�]��autofootnote_start�K�symbol_footnote_start�K �
id_counter��collections��Counter���}���R��parse_messages�]��transform_messages�]�(h�system_message���)��}�(hhh]�h)��}�(hhh]�h�NHyperlink target "sphx-glr-examples-00-load-create-poly-py" is not referenced.�����}�(hhh!j�  ubah1}�(h3]�h5]�h:]�h<]�h>]�uh@hh!j�  ubah1}�(h3]�h5]�h:]�h<]�h>]��level�K�type��INFO��source�hQ�line�K	uh@j�  ubj�  )��}�(hhh]�h)��}�(hhh]�h�5Hyperlink target "ref-create-poly" is not referenced.�����}�(hhh!j�  ubah1}�(h3]�h5]�h:]�h<]�h>]�uh@hh!j�  ubah1}�(h3]�h5]�h:]�h<]�h>]��level�K�type�j�  �source�hQ�line�Kuh@j�  ubj�  )��}�(hhh]�h)��}�(hhh]�h�WHyperlink target "sphx-glr-download-examples-00-load-create-poly-py" is not referenced.�����}�(hhh!j�  ubah1}�(h3]�h5]�h:]�h<]�h>]�uh@hh!j�  ubah1}�(h3]�h5]�h:]�h<]�h>]��level�K�type�j�  �source�hQ�line�KVuh@j�  ube�transformer�N�
decoration�Nhhub.