���l      �sphinx.addnodes��document���)��}�(�	rawsource�� ��children�]�(h �only���)��}�(hhh]�(�docutils.nodes��note���)��}�(h�vClick :ref:`here <sphx_glr_download_examples_02-plot_interpolate-before-map.py>`     to download the full example code�h]�h�	paragraph���)��}�(hhh]�(h�Text����Click �����}�(h�Click ��parent�hubh �pending_xref���)��}�(h�J:ref:`here <sphx_glr_download_examples_02-plot_interpolate-before-map.py>`�h]�h�inline���)��}�(hh&h]�h�here�����}�(hhh!h*uba�
attributes�}�(�ids�]��classes�]�(�xref��std��std-ref�e�names�]��dupnames�]��backrefs�]�u�tagname�h(h!h$ubah1}�(h3]�h5]�h:]�h<]�h>]��refdoc��'examples/02-plot/interpolate-before-map��	refdomain�h8�reftype��ref��refexplicit���refwarn���	reftarget��<sphx_glr_download_examples_02-plot_interpolate-before-map.py�uh@h"�source��|/home/runner/work/pyvista-doc-translations/pyvista-doc-translations/pyvista/docs/examples/02-plot/interpolate-before-map.rst��line�Kh!hubh�&     to download the full example code�����}�(h�&     to download the full example code�h!hubeh1}�(h3]�h5]�h:]�h<]�h>]�uh@hhPhQhRKh!h�uid�� 53879f3443b949239003795220e71c1a�ubah1}�(h3]�h5]��sphx-glr-download-link-note�ah:]�h<]�h>]�uh@hh!hhhhPhQhRNubh�target���)��}�(h�8.. _sphx_glr_examples_02-plot_interpolate-before-map.py:�h]�h1}�(h3]�h5]�h:]�h<]�h>]��refid��3sphx-glr-examples-02-plot-interpolate-before-map-py�uh@hghRK	h!hhhhPhQubeh1}�(h3]�h5]�h:]�h<]�h>]��expr��html�uh@h	hhhPhQhRKh!hubh�section���)��}�(hhh]�(h�title���)��}�(h�Interpolate Before Mapping�h]�h�Interpolate Before Mapping�����}�(hh�h!h�hhhPNhRNubah1}�(h3]�h5]�h:]�h<]�h>]�uh@h�h!hhhhPhQhRKh^� a21e10a5469b44768ba28685683954ef�ubh)��}�(h��The ``add_mesh`` function has an ``interpolate_before_map`` argument - this
affects the way scalar data is visualized with colors.
The effect can of this can vary depending on the dataset's topology and the
chosen colormap.�h]�(h�The �����}�(h�The �h!h�hhhPNhRNubh�literal���)��}�(h�``add_mesh``�h]�h�add_mesh�����}�(hhh!h�ubah1}�(h3]�h5]�h:]�h<]�h>]�uh@h�h!h�ubh� function has an �����}�(h� function has an �h!h�hhhPNhRNubh�)��}�(h�``interpolate_before_map``�h]�h�interpolate_before_map�����}�(hhh!h�ubah1}�(h3]�h5]�h:]�h<]�h>]�uh@h�h!h�ubh�� argument - this
affects the way scalar data is visualized with colors.
The effect can of this can vary depending on the dataset’s topology and the
chosen colormap.�����}�(h�� argument - this
affects the way scalar data is visualized with colors.
The effect can of this can vary depending on the dataset's topology and the
chosen colormap.�h!h�hhhPNhRNubeh1}�(h3]�h5]�h:]�h<]�h>]�uh@hhPhQhRKh!hhhh^� 51b66370effc47d8aca7997bf1652e7d�ubh)��}�(h�ZThis example serves to demo the difference and why we've chosen to enable this
by default.�h]�h�\This example serves to demo the difference and why we’ve chosen to enable this
by default.�����}�(hh�h!h�hhhPNhRNubah1}�(h3]�h5]�h:]�h<]�h>]�uh@hhPhQhRKh!hhhh^� 6a1a2b592fd34ec1811a7e773f5b3c97�ubh)��}�(h�yFor more details, please see `this blog post <https://blog.kitware.com/what-is-interpolatescalarsbeforemapping-in-vtk/>`_�h]�(h�For more details, please see �����}�(h�For more details, please see �h!h�hhhPNhRNubh�	reference���)��}�(h�\`this blog post <https://blog.kitware.com/what-is-interpolatescalarsbeforemapping-in-vtk/>`_�h]�h�this blog post�����}�(h�this blog post�h!h�ubah1}�(h3]�h5]�h:]�h<]�h>]��name��this blog post��refuri��Hhttps://blog.kitware.com/what-is-interpolatescalarsbeforemapping-in-vtk/�uh@h�h!h�ubhh)��}�(h�K <https://blog.kitware.com/what-is-interpolatescalarsbeforemapping-in-vtk/>�h]�h1}�(h3]��this-blog-post�ah5]�h:]��this blog post�ah<]�h>]��refuri�h�uh@hg�
referenced�Kh!h�ubeh1}�(h3]�h5]�h:]�h<]�h>]�uh@hhPhQhRKh!hhhh^� 604b3d71f3eb43979e879f0052c5df58�ubh�literal_block���)��}�(h�:# sphinx_gallery_thumbnail_number = 4
import pyvista as pv�h]�h�:# sphinx_gallery_thumbnail_number = 4
import pyvista as pv�����}�(hhh!j  ubah1}�(h3]�h5]�h:]�h<]�h>]��	xml:space��preserve��force���language��default��highlight_args�}�uh@j  hPhQhRKh!hhhubh)��}�(hX
  Meshes are colored by the data on their nodes or cells - when coloring a mesh
by data on its nodes, the values must be interpolated across the faces of
cells. The process by which those scalars are interpolated is critical.
If the ``interpolate_before_map`` is left off, the color mapping occurs at
polygon points and  colors are interpolated, which is generally less accurate
whereas if the ``interpolate_before_map`` is on, then the scalars will be
interpolated across the topology of the dataset which is more accurate.�h]�(h��Meshes are colored by the data on their nodes or cells - when coloring a mesh
by data on its nodes, the values must be interpolated across the faces of
cells. The process by which those scalars are interpolated is critical.
If the �����}�(h��Meshes are colored by the data on their nodes or cells - when coloring a mesh
by data on its nodes, the values must be interpolated across the faces of
cells. The process by which those scalars are interpolated is critical.
If the �h!j$  hhhPNhRNubh�)��}�(h�``interpolate_before_map``�h]�h�interpolate_before_map�����}�(hhh!j-  ubah1}�(h3]�h5]�h:]�h<]�h>]�uh@h�h!j$  ubh�� is left off, the color mapping occurs at
polygon points and  colors are interpolated, which is generally less accurate
whereas if the �����}�(h�� is left off, the color mapping occurs at
polygon points and  colors are interpolated, which is generally less accurate
whereas if the �h!j$  hhhPNhRNubh�)��}�(h�``interpolate_before_map``�h]�h�interpolate_before_map�����}�(hhh!j@  ubah1}�(h3]�h5]�h:]�h<]�h>]�uh@h�h!j$  ubh�h is on, then the scalars will be
interpolated across the topology of the dataset which is more accurate.�����}�(h�h is on, then the scalars will be
interpolated across the topology of the dataset which is more accurate.�h!j$  hhhPNhRNubeh1}�(h3]�h5]�h:]�h<]�h>]�uh@hhPhQhRK&h!hhhh^� aaf2971a444342399a75e6a40fb9a8d8�ubh)��}�(h��To summarize, when ``interpolate_before_map`` is off, the colors are
interpolated after rendering and when ``interpolate_before_map`` is on, the
scalars are interpolated across the mesh and those values are mapped to
colors.�h]�(h�To summarize, when �����}�(h�To summarize, when �h!jZ  hhhPNhRNubh�)��}�(h�``interpolate_before_map``�h]�h�interpolate_before_map�����}�(hhh!jc  ubah1}�(h3]�h5]�h:]�h<]�h>]�uh@h�h!jZ  ubh�> is off, the colors are
interpolated after rendering and when �����}�(h�> is off, the colors are
interpolated after rendering and when �h!jZ  hhhPNhRNubh�)��}�(h�``interpolate_before_map``�h]�h�interpolate_before_map�����}�(hhh!jv  ubah1}�(h3]�h5]�h:]�h<]�h>]�uh@h�h!jZ  ubh�[ is on, the
scalars are interpolated across the mesh and those values are mapped to
colors.�����}�(h�[ is on, the
scalars are interpolated across the mesh and those values are mapped to
colors.�h!jZ  hhhPNhRNubeh1}�(h3]�h5]�h:]�h<]�h>]�uh@hhPhQhRK.h!hhhh^� 2df49e5db79944feb5161803ccddb4bd�ubh)��}�(h�&So lets take a look at the difference:�h]�h�&So lets take a look at the difference:�����}�(hj�  h!j�  hhhPNhRNubah1}�(h3]�h5]�h:]�h<]�h>]�uh@hhPhQhRK3h!hhhh^� 844377b6feff4707906cbb5c141826ff�ubj  )��}�(hXy  # Load a cylider which has cells with a wide spread
cyl = pv.Cylinder(direction=(0,0,1), height=2).elevation()

# Common display argument to make sure all else is constant
dargs = dict(scalars='Elevation', cmap='rainbow', show_edges=True)

p = pv.Plotter(shape=(1,2))
p.add_mesh(cyl, interpolate_before_map=False,
           stitle='Elevation - not interpolated', **dargs)
p.subplot(0,1)
p.add_mesh(cyl, interpolate_before_map=True,
           stitle='Elevation - interpolated', **dargs)
p.link_views()
p.camera_position = [(-1.67, -5.10, 2.06),
                     (0.0, 0.0, 0.0),
                     (0.00, 0.37, 0.93)]
p.show()�h]�hXy  # Load a cylider which has cells with a wide spread
cyl = pv.Cylinder(direction=(0,0,1), height=2).elevation()

# Common display argument to make sure all else is constant
dargs = dict(scalars='Elevation', cmap='rainbow', show_edges=True)

p = pv.Plotter(shape=(1,2))
p.add_mesh(cyl, interpolate_before_map=False,
           stitle='Elevation - not interpolated', **dargs)
p.subplot(0,1)
p.add_mesh(cyl, interpolate_before_map=True,
           stitle='Elevation - interpolated', **dargs)
p.link_views()
p.camera_position = [(-1.67, -5.10, 2.06),
                     (0.0, 0.0, 0.0),
                     (0.00, 0.37, 0.93)]
p.show()�����}�(hhh!j�  ubah1}�(h3]�h5]�h:]�h<]�h>]�j  j  j  �j   �default�j"  }�uh@j  hPhQhRK6h!hhhubh�image���)��}�(h��.. image:: /examples/02-plot/images/sphx_glr_interpolate-before-map_001.png
    :alt: interpolate before map
    :class: sphx-glr-single-img

�h]�h1}�(h3]�h5]��sphx-glr-single-img�ah:]�h<]�h>]��alt��interpolate before map��uri��?examples/02-plot/images/sphx_glr_interpolate-before-map_001.png��
candidates�}��*�j�  suh@j�  h!hhhhPhQhRNubh)��}�(h�Out:�h]�h�Out:�����}�(hj�  h!j�  hhhPNhRNubah1}�(h3]�h5]��sphx-glr-script-out�ah:]�h<]�h>]�uh@hhPhQhRKUh!hhhh^� 31dbc47ad62b46888a1e7fba16b8aa1f�ubj  )��}�(h�X[(-1.67, -5.1, 2.06),
 (0.0, 0.0, 0.0),
 (0.0, 0.36966744887673536, 0.9291641282577402)]�h]�h�X[(-1.67, -5.1, 2.06),
 (0.0, 0.0, 0.0),
 (0.0, 0.36966744887673536, 0.9291641282577402)]�����}�(hhh!j�  ubah1}�(h3]�h5]�j�  ah:]�h<]�h>]�j  j  j  �j   �none�j"  }�uh@j  hPhQhRKWh!hhhubh)��}�(h��Shown in the figure above, when not interpolating the scalars before mapping,
the colors (RGB values, not scalars) are interpolated between the vertices by
the underlying graphics library (OpenGL), and the colors shown are not
accurate.�h]�h��Shown in the figure above, when not interpolating the scalars before mapping,
the colors (RGB values, not scalars) are interpolated between the vertices by
the underlying graphics library (OpenGL), and the colors shown are not
accurate.�����}�(hj�  h!j�  hhhPNhRNubah1}�(h3]�h5]�h:]�h<]�h>]�uh@hhPhQhRK`h!hhhh^� 75f35ad07975458d8e1a21a4a0a974ef�ubh)��}�(h�EThe same interpolation effect occurs for wireframe visualization too:�h]�h�EThe same interpolation effect occurs for wireframe visualization too:�����}�(hj�  h!j�  hhhPNhRNubah1}�(h3]�h5]�h:]�h<]�h>]�uh@hhPhQhRKeh!hhhh^� 1ee2ad246f2f49f0a46d673830e4dc41�ubj  )��}�(hX)  # Common display argument to make sure all else is constant
dargs = dict(scalars='Elevation', cmap='rainbow', show_edges=True,
             style='wireframe')

p = pv.Plotter(shape=(1,2))
p.add_mesh(cyl, interpolate_before_map=False,
           stitle='Elevation - not interpolated', **dargs)
p.subplot(0,1)
p.add_mesh(cyl, interpolate_before_map=True,
           stitle='Elevation - interpolated', **dargs)
p.link_views()
p.camera_position = [(-1.67, -5.10, 2.06),
                     (0.0, 0.0, 0.0),
                     (0.00, 0.37, 0.93)]
p.show()�h]�hX)  # Common display argument to make sure all else is constant
dargs = dict(scalars='Elevation', cmap='rainbow', show_edges=True,
             style='wireframe')

p = pv.Plotter(shape=(1,2))
p.add_mesh(cyl, interpolate_before_map=False,
           stitle='Elevation - not interpolated', **dargs)
p.subplot(0,1)
p.add_mesh(cyl, interpolate_before_map=True,
           stitle='Elevation - interpolated', **dargs)
p.link_views()
p.camera_position = [(-1.67, -5.10, 2.06),
                     (0.0, 0.0, 0.0),
                     (0.00, 0.37, 0.93)]
p.show()�����}�(hhh!j  ubah1}�(h3]�h5]�h:]�h<]�h>]�j  j  j  �j   �default�j"  }�uh@j  hPhQhRKhh!hhhubj�  )��}�(h��.. image:: /examples/02-plot/images/sphx_glr_interpolate-before-map_002.png
    :alt: interpolate before map
    :class: sphx-glr-single-img

�h]�h1}�(h3]�h5]��sphx-glr-single-img�ah:]�h<]�h>]��alt��interpolate before map��uri��?examples/02-plot/images/sphx_glr_interpolate-before-map_002.png�j�  }�j�  j  suh@j�  h!hhhhPhQhRNubh)��}�(h�Out:�h]�h�Out:�����}�(hj#  h!j!  hhhPNhRNubah1}�(h3]�h5]��sphx-glr-script-out�ah:]�h<]�h>]�uh@hhPhQhRK�h!hhhh^� 405647c712d7458287848dd1be034f3a�ubj  )��}�(h�X[(-1.67, -5.1, 2.06),
 (0.0, 0.0, 0.0),
 (0.0, 0.36966744887673536, 0.9291641282577402)]�h]�h�X[(-1.67, -5.1, 2.06),
 (0.0, 0.0, 0.0),
 (0.0, 0.36966744887673536, 0.9291641282577402)]�����}�(hhh!j1  ubah1}�(h3]�h5]�j,  ah:]�h<]�h>]�j  j  j  �j   �none�j"  }�uh@j  hPhQhRK�h!hhhubh)��}�(h��The cylider mesh above is a great example dataset for this as it has a wide
spread between the vertices (points are only at the top and bottom of the
cylinder) which means high surface are of the mesh has to be interpolated.�h]�h��The cylider mesh above is a great example dataset for this as it has a wide
spread between the vertices (points are only at the top and bottom of the
cylinder) which means high surface are of the mesh has to be interpolated.�����}�(hjC  h!jA  hhhPNhRNubah1}�(h3]�h5]�h:]�h<]�h>]�uh@hhPhQhRK�h!hhhh^� edff7f7f228549da944371a43c0dd1b2�ubh)��}�(h��However, most meshes don't have such a wide spread and the effects of
color interpolating are harder to notice. Let's take a look at a wavelet
example and try to figure out how the ``interpolate_before_map`` option
affects its rendering.�h]�(h��However, most meshes don’t have such a wide spread and the effects of
color interpolating are harder to notice. Let’s take a look at a wavelet
example and try to figure out how the �����}�(h��However, most meshes don't have such a wide spread and the effects of
color interpolating are harder to notice. Let's take a look at a wavelet
example and try to figure out how the �h!jP  hhhPNhRNubh�)��}�(h�``interpolate_before_map``�h]�h�interpolate_before_map�����}�(hhh!jY  ubah1}�(h3]�h5]�h:]�h<]�h>]�uh@h�h!jP  ubh� option
affects its rendering.�����}�(h� option
affects its rendering.�h!jP  hhhPNhRNubeh1}�(h3]�h5]�h:]�h<]�h>]�uh@hhPhQhRK�h!hhhh^� b64837aacacc4cb59f38a64d8541993b�ubj  )��}�(hX&  wavelet = pv.Wavelet().clip('x')

# Common display argument to make sure all else is constant
dargs = dict(scalars='RTData', cmap='rainbow', show_edges=True)

p = pv.Plotter(shape=(1,2))
p.add_mesh(wavelet, interpolate_before_map=False,
           stitle='RTData - not interpolated', **dargs)
p.subplot(0,1)
p.add_mesh(wavelet, interpolate_before_map=True,
           stitle='RTData - interpolated', **dargs)
p.link_views()
p.camera_position = [(55., 16, 31),
                     (-5.0, 0.0, 0.0),
                     (-0.22, 0.97, -0.09)]
p.show()�h]�hX&  wavelet = pv.Wavelet().clip('x')

# Common display argument to make sure all else is constant
dargs = dict(scalars='RTData', cmap='rainbow', show_edges=True)

p = pv.Plotter(shape=(1,2))
p.add_mesh(wavelet, interpolate_before_map=False,
           stitle='RTData - not interpolated', **dargs)
p.subplot(0,1)
p.add_mesh(wavelet, interpolate_before_map=True,
           stitle='RTData - interpolated', **dargs)
p.link_views()
p.camera_position = [(55., 16, 31),
                     (-5.0, 0.0, 0.0),
                     (-0.22, 0.97, -0.09)]
p.show()�����}�(hhh!js  ubah1}�(h3]�h5]�h:]�h<]�h>]�j  j  j  �j   �default�j"  }�uh@j  hPhQhRK�h!hhhubj�  )��}�(h��.. image:: /examples/02-plot/images/sphx_glr_interpolate-before-map_003.png
    :alt: interpolate before map
    :class: sphx-glr-single-img

�h]�h1}�(h3]�h5]��sphx-glr-single-img�ah:]�h<]�h>]��alt��interpolate before map��uri��?examples/02-plot/images/sphx_glr_interpolate-before-map_003.png�j�  }�j�  j�  suh@j�  h!hhhhPhQhRNubh)��}�(h�Out:�h]�h�Out:�����}�(hj�  h!j�  hhhPNhRNubah1}�(h3]�h5]��sphx-glr-script-out�ah:]�h<]�h>]�uh@hhPhQhRK�h!hhhh^� 003362782e884880971ce12d7bd33bab�ubj  )��}�(h�i[(55.0, 16.0, 31.0),
 (-5.0, 0.0, 0.0),
 (-0.22028655891110546, 0.971263464289874, -0.09011722864545223)]�h]�h�i[(55.0, 16.0, 31.0),
 (-5.0, 0.0, 0.0),
 (-0.22028655891110546, 0.971263464289874, -0.09011722864545223)]�����}�(hhh!j�  ubah1}�(h3]�h5]�j�  ah:]�h<]�h>]�j  j  j  �j   �none�j"  }�uh@j  hPhQhRK�h!hhhubh)��}�(hX  This time is pretty difficult to notice the differences - they are there,
subtle, but present. The differences become more apperant when we decrease
the number of colors in colormap.
Let's take a look at the differences when using eight discrete colors via
the ``n_colors`` argument:�h]�(hX  This time is pretty difficult to notice the differences - they are there,
subtle, but present. The differences become more apperant when we decrease
the number of colors in colormap.
Let’s take a look at the differences when using eight discrete colors via
the �����}�(hX  This time is pretty difficult to notice the differences - they are there,
subtle, but present. The differences become more apperant when we decrease
the number of colors in colormap.
Let's take a look at the differences when using eight discrete colors via
the �h!j�  hhhPNhRNubh�)��}�(h�``n_colors``�h]�h�n_colors�����}�(hhh!j�  ubah1}�(h3]�h5]�h:]�h<]�h>]�uh@h�h!j�  ubh�
 argument:�����}�(h�
 argument:�h!j�  hhhPNhRNubeh1}�(h3]�h5]�h:]�h<]�h>]�uh@hhPhQhRK�h!hhhh^� 5721057a82894ae7b1fcc01e842d7b71�ubj  )��}�(hX�  dargs = dict(scalars='RTData', cmap='rainbow', show_edges=True, n_colors=8)

p = pv.Plotter(shape=(1,2))
p.add_mesh(wavelet, interpolate_before_map=False,
           stitle='RTData - not interpolated', **dargs)
p.subplot(0,1)
p.add_mesh(wavelet, interpolate_before_map=True,
           stitle='RTData - interpolated', **dargs)
p.link_views()
p.camera_position = [(55., 16, 31),
                     (-5.0, 0.0, 0.0),
                     (-0.22, 0.97, -0.09)]
p.show()�h]�hX�  dargs = dict(scalars='RTData', cmap='rainbow', show_edges=True, n_colors=8)

p = pv.Plotter(shape=(1,2))
p.add_mesh(wavelet, interpolate_before_map=False,
           stitle='RTData - not interpolated', **dargs)
p.subplot(0,1)
p.add_mesh(wavelet, interpolate_before_map=True,
           stitle='RTData - interpolated', **dargs)
p.link_views()
p.camera_position = [(55., 16, 31),
                     (-5.0, 0.0, 0.0),
                     (-0.22, 0.97, -0.09)]
p.show()�����}�(hhh!j�  ubah1}�(h3]�h5]�h:]�h<]�h>]�j  j  j  �j   �default�j"  }�uh@j  hPhQhRK�h!hhhubj�  )��}�(h��.. image:: /examples/02-plot/images/sphx_glr_interpolate-before-map_004.png
    :alt: interpolate before map
    :class: sphx-glr-single-img

�h]�h1}�(h3]�h5]��sphx-glr-single-img�ah:]�h<]�h>]��alt��interpolate before map��uri��?examples/02-plot/images/sphx_glr_interpolate-before-map_004.png�j�  }�j�  j�  suh@j�  h!hhhhPhQhRNubh)��}�(h�Out:�h]�h�Out:�����}�(hj�  h!j�  hhhPNhRNubah1}�(h3]�h5]��sphx-glr-script-out�ah:]�h<]�h>]�uh@hhPhQhRK�h!hhhh^� 58ef8a1a0eaa4e7fa9dabb88265253a3�ubj  )��}�(h�i[(55.0, 16.0, 31.0),
 (-5.0, 0.0, 0.0),
 (-0.22028655891110546, 0.971263464289874, -0.09011722864545223)]�h]�h�i[(55.0, 16.0, 31.0),
 (-5.0, 0.0, 0.0),
 (-0.22028655891110546, 0.971263464289874, -0.09011722864545223)]�����}�(hhh!j  ubah1}�(h3]�h5]�j  ah:]�h<]�h>]�j  j  j  �j   �none�j"  }�uh@j  hPhQhRK�h!hhhubh)��}�(h�LLeft, ``interpolate_before_map`` OFF.  Right, ``interpolate_before_map`` ON.�h]�(h�Left, �����}�(h�Left, �h!j  hhhPNhRNubh�)��}�(h�``interpolate_before_map``�h]�h�interpolate_before_map�����}�(hhh!j  ubah1}�(h3]�h5]�h:]�h<]�h>]�uh@h�h!j  ubh� OFF.  Right, �����}�(h� OFF.  Right, �h!j  hhhPNhRNubh�)��}�(h�``interpolate_before_map``�h]�h�interpolate_before_map�����}�(hhh!j2  ubah1}�(h3]�h5]�h:]�h<]�h>]�uh@h�h!j  ubh� ON.�����}�(h� ON.�h!j  hhhPNhRNubeh1}�(h3]�h5]�h:]�h<]�h>]�uh@hhPhQhRK�h!hhhh^� 9d00545a3356461eaff8e1279949d42b�ubh)��}�(hX(  Now that is much more compelling! On the right, the contours of the scalar
field are visible, but on the left, the contours are obscured due to the color
interpolation by OpenGL. In both cases, the colors at the vertices are the
same, the difference is how color is assigned between the vertices.�h]�hX(  Now that is much more compelling! On the right, the contours of the scalar
field are visible, but on the left, the contours are obscured due to the color
interpolation by OpenGL. In both cases, the colors at the vertices are the
same, the difference is how color is assigned between the vertices.�����}�(hjN  h!jL  hhhPNhRNubah1}�(h3]�h5]�h:]�h<]�h>]�uh@hhPhQhRK�h!hhhh^� dabffade78cc459b9019aa209d028fa1�ubh)��}�(h��In our opinion, color interpolation is not a preferred default for scientific
visualization and is why we have chosen to set the ``interpolate_before_map``
flag to ``True``.�h]�(h��In our opinion, color interpolation is not a preferred default for scientific
visualization and is why we have chosen to set the �����}�(h��In our opinion, color interpolation is not a preferred default for scientific
visualization and is why we have chosen to set the �h!j[  hhhPNhRNubh�)��}�(h�``interpolate_before_map``�h]�h�interpolate_before_map�����}�(hhh!jd  ubah1}�(h3]�h5]�h:]�h<]�h>]�uh@h�h!j[  ubh�	
flag to �����}�(h�	
flag to �h!j[  hhhPNhRNubh�)��}�(h�``True``�h]�h�True�����}�(hhh!jw  ubah1}�(h3]�h5]�h:]�h<]�h>]�uh@h�h!j[  ubh�.�����}�(h�.�h!j[  hhhPNhRNubeh1}�(h3]�h5]�h:]�h<]�h>]�uh@hhPhQhRK�h!hhhh^� 7af7a2a4886b4210baed00897a4b70d6�ubh)��}�(h�B**Total running time of the script:** ( 0 minutes  10.119 seconds)�h]�(h�strong���)��}�(h�%**Total running time of the script:**�h]�h�!Total running time of the script:�����}�(hhh!j�  ubah1}�(h3]�h5]�h:]�h<]�h>]�uh@j�  h!j�  ubh� ( 0 minutes  10.119 seconds)�����}�(h� ( 0 minutes  10.119 seconds)�h!j�  hhhPNhRNubeh1}�(h3]�h5]��sphx-glr-timing�ah:]�h<]�h>]�uh@hhPhQhRK�h!hhhh^� f20e385451254d599c8d8bdc7c45bd6d�ubhh)��}�(h�A.. _sphx_glr_download_examples_02-plot_interpolate-before-map.py:�h]�h1}�(h3]�h5]�h:]�h<]�h>]�hs�<sphx-glr-download-examples-02-plot-interpolate-before-map-py�uh@hghRM h!hhhhPhQubh
)��}�(hhh]�h�	container���)��}�(hXA  .. container:: sphx-glr-download sphx-glr-download-python

   :download:`Download Python source code: interpolate-before-map.py <interpolate-before-map.py>`



.. container:: sphx-glr-download sphx-glr-download-jupyter

   :download:`Download Jupyter notebook: interpolate-before-map.ipynb <interpolate-before-map.ipynb>`�h]�(j�  )��}�(h�^:download:`Download Python source code: interpolate-before-map.py <interpolate-before-map.py>`�h]�h)��}�(hj�  h]�h �download_reference���)��}�(hj�  h]�h�)��}�(hj�  h]�h�6Download Python source code: interpolate-before-map.py�����}�(hhh!j�  ubah1}�(h3]�h5]�(h7�download�eh:]�h<]�h>]�uh@h�h!j�  ubah1}�(h3]�h5]�h:]�h<]�h>]��refdoc�hH�	refdomain�h�reftype�j�  �refexplicit���refwarn��hN�interpolate-before-map.py��filename��:91cd0be2efaed168b131785c026dee9f/interpolate-before-map.py�uh@j�  hPhQhRMh!j�  ubah1}�(h3]�h5]�h:]�h<]�h>]�uh@hhPhQhRMh!j�  h^� eacfa299117945bdba7e8583d11902d2�ubah1}�(h3]�h5]�(�sphx-glr-download��sphx-glr-download-python�eh:]�h<]�h>]�uh@j�  h!j�  ubj�  )��}�(h�b:download:`Download Jupyter notebook: interpolate-before-map.ipynb <interpolate-before-map.ipynb>`�h]�h)��}�(hj�  h]�j�  )��}�(hj�  h]�h�)��}�(hj�  h]�h�7Download Jupyter notebook: interpolate-before-map.ipynb�����}�(hhh!j  ubah1}�(h3]�h5]�(h7�download�eh:]�h<]�h>]�uh@h�h!j  ubah1}�(h3]�h5]�h:]�h<]�h>]��refdoc�hH�	refdomain�h�reftype�j  �refexplicit���refwarn��hN�interpolate-before-map.ipynb�j�  �=0bf1c8f3563829244ab0f63ecd8d0e56/interpolate-before-map.ipynb�uh@j�  hPhQhRMh!j  ubah1}�(h3]�h5]�h:]�h<]�h>]�uh@hhPhQhRMh!j�  h^� d101310e359141d1b3c97c6bc2e6b3bf�ubah1}�(h3]�h5]�(�sphx-glr-download��sphx-glr-download-jupyter�eh:]�h<]�h>]�uh@j�  h!j�  ubeh1}�(h3]�h5]�(�sphx-glr-footer��class��sphx-glr-footer-example�eh:]�h<]�h>]�uh@j�  h!j�  hhhPNhRNubah1}�(h3]�j�  ah5]�h:]��<sphx_glr_download_examples_02-plot_interpolate-before-map.py�ah<]�h>]�h{�html�uh@h	hhhPhQhRMh!h�expect_referenced_by_name�}�j>  j�  s�expect_referenced_by_id�}�j�  j�  subh
)��}�(hhh]�h)��}�(h�I`Gallery generated by Sphinx-Gallery <https://sphinx-gallery.github.io>`_�h]�(h�)��}�(hjK  h]�h�#Gallery generated by Sphinx-Gallery�����}�(h�#Gallery generated by Sphinx-Gallery�h!jM  ubah1}�(h3]�h5]�h:]�h<]�h>]��name��#Gallery generated by Sphinx-Gallery�h�� https://sphinx-gallery.github.io�uh@h�h!jI  ubhh)��}�(h�# <https://sphinx-gallery.github.io>�h]�h1}�(h3]��#gallery-generated-by-sphinx-gallery�ah5]�h:]��#gallery generated by sphinx-gallery�ah<]�h>]��refuri�j]  uh@hgj  Kh!jI  ubeh1}�(h3]�h5]��sphx-glr-signature�ah:]�h<]�h>]�uh@hhPhQhRMh!jF  hhh^� ccdafea219da45e785267e26cd7c5482�ubah1}�(h3]�h5]�h:]�h<]�h>]�h{�html�uh@h	hhhPhQhRMh!hubeh1}�(h3]�(�interpolate-before-mapping�hteh5]��sphx-glr-example-title�ah:]�(�interpolate before mapping��3sphx_glr_examples_02-plot_interpolate-before-map.py�eh<]�h>]�uh@h}h!hhhhPhQhRKjB  }�j�  hisjD  }�hthisubeh1}�(h3]�h5]�h:]�h<]�h>]��source�hQuh@h�current_source�N�current_line�N�settings��docutils.frontend��Values���)��}�(h�N�	generator�N�	datestamp�N�source_link�N�
source_url�N�toc_backlinks��entry��footnote_backlinks�K�sectnum_xform�K�strip_comments�N�strip_elements_with_classes�N�strip_classes�N�report_level�K�
halt_level�K�exit_status_level�K�debug�N�warning_stream�N�	traceback���input_encoding��	utf-8-sig��input_encoding_error_handler��strict��output_encoding��utf-8��output_encoding_error_handler�j�  �error_encoding��UTF-8��error_encoding_error_handler��backslashreplace��language_code��en��record_dependencies�N�config�N�	id_prefix�h�auto_id_prefix��id��dump_settings�N�dump_internals�N�dump_transforms�N�dump_pseudo_xml�N�expose_internals�N�strict_visitor�N�_disable_config�N�_source�hQ�_destination�N�_config_files�]��pep_references�N�pep_base_url�� https://www.python.org/dev/peps/��pep_file_url_template��pep-%04d��rfc_references�N�rfc_base_url��https://tools.ietf.org/html/��	tab_width�K�trim_footnote_reference_space���file_insertion_enabled���raw_enabled�K�syntax_highlight��long��smart_quotes���smartquotes_locales�]��character_level_inline_markup���doctitle_xform���docinfo_xform�K�sectsubtitle_xform���embed_stylesheet���cloak_email_addresses���env�Nub�reporter�N�indirect_targets�]��substitution_defs�}��substitution_names�}��refnames�}��refids�}�(ht]�hiaj�  ]�j�  au�nameids�}�(j�  htj�  j|  j  h�j>  j�  jg  jd  u�	nametypes�}�(j�  �j�  Nj  �j>  �jg  �uh3}�(hthj|  hh�h�j�  j�  jd  j^  u�footnote_refs�}��citation_refs�}��autofootnotes�]��autofootnote_refs�]��symbol_footnotes�]��symbol_footnote_refs�]��	footnotes�]��	citations�]��autofootnote_start�K�symbol_footnote_start�K �
id_counter��collections��Counter���}���R��parse_messages�]��transform_messages�]�(h�system_message���)��}�(hhh]�h)��}�(hhh]�h�YHyperlink target "sphx-glr-examples-02-plot-interpolate-before-map-py" is not referenced.�����}�(hhh!j  ubah1}�(h3]�h5]�h:]�h<]�h>]�uh@hh!j  ubah1}�(h3]�h5]�h:]�h<]�h>]��level�K�type��INFO��source�hQ�line�K	uh@j
  ubj  )��}�(hhh]�h)��}�(hhh]�h�bHyperlink target "sphx-glr-download-examples-02-plot-interpolate-before-map-py" is not referenced.�����}�(hhh!j*  ubah1}�(h3]�h5]�h:]�h<]�h>]�uh@hh!j'  ubah1}�(h3]�h5]�h:]�h<]�h>]��level�K�type�j$  �source�hQ�line�M uh@j
  ube�transformer�N�
decoration�Nhhub.