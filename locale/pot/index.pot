# SOME DESCRIPTIVE TITLE.
# Copyright (C) 2017-2020, The PyVista Developers
# This file is distributed under the same license as the PyVista package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PyVista 0.26.b0\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2020-09-08 00:54+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../pyvista/docs/index.rst:191
#: ../../pyvista/docs/index.rst:172
msgid "Getting Started"
msgstr ""

#: ../../pyvista/docs/index.rst:235
#: ../../pyvista/docs/index.rst:224
msgid "API Reference"
msgstr ""

#: ../../pyvista/docs/index.rst:50
msgid "Deployment"
msgstr ""

#: ../../pyvista/docs/index.rst:50
msgid "|pypi| |conda|"
msgstr ""

#: ../../pyvista/docs/index.rst:52
msgid "Build Status"
msgstr ""

#: ../../pyvista/docs/index.rst:52
msgid "|azure|"
msgstr ""

#: ../../pyvista/docs/index.rst:54
msgid "Metrics"
msgstr ""

#: ../../pyvista/docs/index.rst:54
msgid "|codacy| |codecov|"
msgstr ""

#: ../../pyvista/docs/index.rst:56
msgid "GitHub"
msgstr ""

#: ../../pyvista/docs/index.rst:56
msgid "|contributors| |stars|"
msgstr ""

#: ../../pyvista/docs/index.rst:58
msgid "Citation"
msgstr ""

#: ../../pyvista/docs/index.rst:58
msgid "|joss| |zenodo|"
msgstr ""

#: ../../pyvista/docs/index.rst:60
msgid "License"
msgstr ""

#: ../../pyvista/docs/index.rst:60
msgid "|MIT|"
msgstr ""

#: ../../pyvista/docs/index.rst:62
msgid "Community"
msgstr ""

#: ../../pyvista/docs/index.rst:62
msgid "|slack| |gitter|"
msgstr ""

#: ../../pyvista/docs/index.rst:67
msgid "About"
msgstr ""

#: ../../pyvista/docs/index.rst:69
msgid "PyVista is..."
msgstr ""

#: ../../pyvista/docs/index.rst:71
msgid "*\"VTK for humans\"*: a high-level API to the `Visualization Toolkit`_ (VTK)"
msgstr ""

#: ../../pyvista/docs/index.rst:72
msgid "mesh data structures and filtering methods for spatial datasets"
msgstr ""

#: ../../pyvista/docs/index.rst:73
msgid "3D plotting made simple and built for large/complex data geometries"
msgstr ""

#: ../../pyvista/docs/index.rst:78
msgid "PyVista (formerly ``vtki``) is a helper module for the Visualization Toolkit (VTK) that takes a different approach on interfacing with VTK through NumPy and direct array access. This package provides a Pythonic, well-documented interface exposing VTK's powerful visualization backend to facilitate rapid prototyping, analysis, and visual integration of spatially referenced datasets."
msgstr ""

#: ../../pyvista/docs/index.rst:85
msgid "This module can be used for scientific plotting for presentations and research papers as well as a supporting module for other mesh dependent Python modules."
msgstr ""

#: ../../pyvista/docs/index.rst:91
msgid "Share this project on Twitter: |tweet|"
msgstr ""

#: ../../pyvista/docs/index.rst:98
msgid "Want to test-drive PyVista? Check out our live examples on MyBinder: |binder|"
msgstr ""

#: ../../pyvista/docs/index.rst:110
msgid "Support"
msgstr ""

#: ../../pyvista/docs/index.rst:112
msgid "For general questions about the project, its applications, or about software usage, please create an issue in the `pyvista/pyvista-support`_ repository where the community can collectively address your questions. You are also welcome to join us on Slack_ or send one of the developers an email. The project support team can be reached at `info@pyvista.org`_."
msgstr ""

#: ../../pyvista/docs/index.rst:124
msgid "Connections"
msgstr ""

#: ../../pyvista/docs/index.rst:126
msgid "PyVista is a powerful tool that researchers can harness to create compelling, integrated visualizations of large datasets in an intuitive, Pythonic manner. Here are a few open-source projects that leverage PyVista:"
msgstr ""

#: ../../pyvista/docs/index.rst:130
msgid "itkwidgets_: Interactive Jupyter widgets to visualize images, point sets, and meshes in 2D and 3D. Supports all PyVista mesh types."
msgstr ""

#: ../../pyvista/docs/index.rst:131
msgid "pyansys_: Pythonic interface to ANSYS result, full, and archive files"
msgstr ""

#: ../../pyvista/docs/index.rst:132
msgid "PVGeo_: Python package of VTK-based algorithms to analyze geoscientific data and models. PyVista is used to make the inputs and outputs of PVGeo's algorithms more accessible."
msgstr ""

#: ../../pyvista/docs/index.rst:133
msgid "omfvista_: 3D visualization for the Open Mining Format (omf). PyVista provides the foundation for this library's visualization."
msgstr ""

#: ../../pyvista/docs/index.rst:134
msgid "discretize_: Discretization tools for finite volume and inverse problems. ``discretize`` provides ``toVTK`` methods that return PyVista versions of their data types for `creating compelling visualizations`_."
msgstr ""

#: ../../pyvista/docs/index.rst:135
msgid "pymeshfix_: Python/Cython wrapper of Marco Attene's wonderful, award-winning MeshFix software."
msgstr ""

#: ../../pyvista/docs/index.rst:136
msgid "tetgen_: Python Interface to Hang Si's C++ TetGen Library"
msgstr ""

#: ../../pyvista/docs/index.rst:151
msgid "Citing PyVista"
msgstr ""

#: ../../pyvista/docs/index.rst:153
msgid "There is a `paper about PyVista <https://doi.org/10.21105/joss.01450>`_!"
msgstr ""

#: ../../pyvista/docs/index.rst:155
msgid "If you are using PyVista in your scientific research, please help our scientific visibility by citing our work! Head over to :ref:`citation_ref` to learn more about citing PyVista."
msgstr ""

#: ../../pyvista/docs/index.rst:160
msgid "Videos"
msgstr ""

#: ../../pyvista/docs/index.rst:162
msgid "Here are some videos that you can watch to learn pyvista:"
msgstr ""

#: ../../pyvista/docs/index.rst:164
msgid "PyConJP2020 talk \"How to plot unstructured mesh file on Jupyter Notebook\" (15 minutes):"
msgstr ""

#: ../../pyvista/docs/index.rst:166
msgid "Video: https://youtu.be/7HCJD4oxfuo?t=15928"
msgstr ""

#: ../../pyvista/docs/index.rst:167
msgid "Material: https://docs.google.com/presentation/d/1M_cnS66ja81u_mHACjaUsDj1wSeeEtnEevk_IMZ8-dg/edit?usp=sharing"
msgstr ""

#: ../../pyvista/docs/index.rst:169
msgid "If there is any material that we can add, please  `report <https://github.com/pyvista/pyvista/issues>`_ ."
msgstr ""

#: ../../pyvista/docs/index.rst:174
msgid "If you have a working copy of VTK, installation is simply::"
msgstr ""

#: ../../pyvista/docs/index.rst:178
msgid "You can also visit `PyPi <https://pypi.org/project/pyvista/>`_ or `GitHub <https://github.com/pyvista/pyvista>`_ to download the source."
msgstr ""

#: ../../pyvista/docs/index.rst:181
msgid "See :ref:`install_ref` for more details."
msgstr ""

#: ../../pyvista/docs/index.rst:184
msgid "Be sure to head over to the `examples gallery <./examples/index.html>`_ to explore different use cases of PyVista and to start visualizing 3D data in Python! Also, please explore the list of external projects leveraging PyVista for 3D visualization in our `external examples list <./external_examples.html>`_"
msgstr ""

#: ../../pyvista/docs/index.rst:204
msgid "Translating the documentation"
msgstr ""

#: ../../pyvista/docs/index.rst:206
msgid "The recommended way for new contributors to translate ``pyvista``'s documentation is to join the translation team on Transifex."
msgstr ""

#: ../../pyvista/docs/index.rst:209
msgid "There is `pyvista translation page`_ for pyvista (master) documentation."
msgstr ""

#: ../../pyvista/docs/index.rst:211
msgid "Login to transifex_ service."
msgstr ""

#: ../../pyvista/docs/index.rst:212
msgid "Go to `pyvista translation page`_."
msgstr ""

#: ../../pyvista/docs/index.rst:213
msgid "Click ``Request language`` and fill form."
msgstr ""

#: ../../pyvista/docs/index.rst:214
msgid "Wait acceptance by transifex pyvista translation maintainers."
msgstr ""

#: ../../pyvista/docs/index.rst:215
msgid "(After acceptance) Translate on transifex."
msgstr ""

#: ../../pyvista/docs/index.rst:216
msgid "You can see the translated document in `Read The Docs`_."
msgstr ""

#: ../../pyvista/docs/index.rst:226
msgid "In this section, you can learn more about how PyVista wraps different VTK mesh types and how you can leverage powerful 3D plotting and mesh analysis tools. Highlights of the API include:"
msgstr ""

#: ../../pyvista/docs/index.rst:230
msgid "Pythonic interface to VTK's Python-C++ bindings"
msgstr ""

#: ../../pyvista/docs/index.rst:231
msgid "Filtering/plotting tools built for interactivity (see :ref:`widgets`)"
msgstr ""

#: ../../pyvista/docs/index.rst:232
msgid "Direct access to common VTK filters (see :ref:`filters_ref`)"
msgstr ""

#: ../../pyvista/docs/index.rst:233
msgid "Intuitive plotting routines with ``matplotlib`` similar syntax (see :ref:`plotting_ref`)"
msgstr ""

#: ../../pyvista/docs/index.rst:248
msgid "Project Index"
msgstr ""

#: ../../pyvista/docs/index.rst:250
msgid ":ref:`genindex`"
msgstr ""
